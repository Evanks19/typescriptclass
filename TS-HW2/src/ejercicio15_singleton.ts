namespace ejercicio15_singleton{
    // Singleton________________________________
class Logger {
  private static instance: Logger | null;

  private constructor() {}

  static getInstance(): Logger {
    if (!Logger.instance) {
      Logger.instance = new Logger();
    }
    return Logger.instance;
  }

  log(message: string): void {
    console.log(`[LOG] ${message}`);
  }
}

const logger1 = Logger.getInstance();
const logger2 = Logger.getInstance();

logger1.log("Mensaje de prueba");

console.log(logger1 === logger2);
}