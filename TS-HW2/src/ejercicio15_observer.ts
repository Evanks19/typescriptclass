namespace ejercicio15_observer {
  // observer_________________________

  interface Observer {
    update(data: any): void;
  }

  class Observable {
    private observers: Observer[] = [];

    addObserver(observer: Observer): void {
      this.observers.push(observer);
    }

    removeObserver(observer: Observer): void {
      this.observers = this.observers.filter((obs) => obs !== observer);
    }

    notifyObservers(data: any): void {
      this.observers.forEach((observer) => {
        observer.update(data);
      });
    }
  }

  class ConcreteObserver implements Observer {
    update(data: any): void {
      console.log(`Se ha producido un cambio: ${data}`);
    }
  }

  const observable = new Observable();
  const observer1 = new ConcreteObserver();
  const observer2 = new ConcreteObserver();

  observable.addObserver(observer1);
  observable.addObserver(observer2);

  observable.notifyObservers('datos actualizados');
  observable.notifyObservers('datos actualizados nuevamente');
}
