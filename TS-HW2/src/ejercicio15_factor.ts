namespace ejercicio15_factor {
  // // factoru method__________________________________

  abstract class Vehicle {
    abstract drive(): void;
  }

  class Car extends Vehicle {
    drive(): void {
      console.log('Conduciendo un coche...');
    }
  }

  class Bicycle extends Vehicle {
    drive(): void {
      console.log('Montando en bicicleta...');
    }
  }

  class VehicleFactory {
    createVehicle(type: string): Vehicle {
      if (type === 'car') {
        return new Car();
      } else if (type === 'bicycle') {
        return new Bicycle();
      } else {
        throw new Error('Tipo de vehículo no válido.');
      }
    }
  }

  const factory = new VehicleFactory();

  const car = factory.createVehicle('car');
  car.drive();

  const bicycle = factory.createVehicle('bicycle');
  bicycle.drive();
}
