import { Note } from './../models/notes';
import { User } from '../models/user';
import fs from 'fs';
import path from 'path';
import { logDecorator } from '../utilities/decorators';

const userFilePath = path.resolve(__dirname, '../../data/users.json');

interface InterUser {
  id?: number;
  name?: string;

  lastname?: string;

  email?: string;
}

export class UserApp {
  private users: User[] = [];

  constructor() {
    this.loadUsersFromFile();
  }

  private loadUsersFromFile() {
    try {
      const data = fs.readFileSync(userFilePath, 'utf-8');
      this.users = JSON.parse(data);
    } catch (error) {
      this.users = [];
    }
  }

  private saveUsersToFile() {
    fs.writeFileSync(userFilePath, JSON.stringify(this.users, null, 2));
  }

  @logDecorator()
  async createUser(
    _id: number,
    _name: string,
    _lastname: string,
    _email: string
  ): Promise<InterUser | undefined> {
    try {
      const newUser: User = new User(_id, _name, _lastname, _email);

      const { id, name, lastname, email } = newUser;

      const savedUser = { id, name, lastname, email };

      this.users.push(savedUser);
      this.saveUsersToFile();

      return savedUser;
    } catch (error: any) {
      console.log(error.message);
    }
  }

  async readUsers(): Promise<User[] | null | []> {
    return this.users;
  }

  @logDecorator()
  async updateUser(
    userId: number,
    updatedUser: InterUser
  ): Promise<string | undefined> {
    try {
      const index = this.users.findIndex((user) => user.id === userId);
      if (index !== -1) {
        const { name, lastname, email } = updatedUser;

        if (name) this.users[index].name = name;
        if (lastname) this.users[index].lastname = lastname;
        if (email) this.users[index].email = email;

        this.saveUsersToFile();
        return `Nota con ID ${userId} actualizada.`;
      } else {
        throw new Error(`No se encontró ningun user con el ID ${userId}`);
      }
    } catch (error: any) {
      console.log(error.message);
    }
  }

  @logDecorator()
  async deleteUser(userId: number): Promise<string | undefined> {
    try {
      const index = this.users.findIndex((user) => user.id === userId);
      if (index !== -1) {
        this.users.splice(index, 1);
        this.saveUsersToFile();
        return `User con ID ${userId} eliminado.`;
      } else {
        throw new Error(`No se encontró ningun user con el ID ${userId}`);
      }
    } catch (error: any) {
      console.log(error.message);
    }
  }
}
