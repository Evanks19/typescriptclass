import fs from 'fs';
import path from 'path';
import { Note } from '../models/notes';
import { logDecorator, required, validate } from '../utilities/decorators';

const notesFilePath = path.resolve(__dirname, '../../data/notes.json');

interface interNotes {
  id?: number;

  title?: string;

  description?: string;

  userId?: number;

  dueDate?: Date;
}

export class NotesApp {
  private notes: Note[] = [];

  constructor() {
    this.loadNotesFromFile();
  }

  private loadNotesFromFile() {
    try {
      const data = fs.readFileSync(notesFilePath, 'utf-8');
      this.notes = JSON.parse(data);
    } catch (error) {
      this.notes = [];
    }
  }

  private saveNotesToFile() {
    fs.writeFileSync(notesFilePath, JSON.stringify(this.notes, null, 2));
  }

  @validate
  @logDecorator()
  async createNote(
    _id: number,
    @required _title: string,
    _description: string,
    _userId: number
  ): Promise<interNotes | undefined> {
    try {
      const newNote: Note = new Note(
        _id,
        _title,
        _description,
        _userId,
        new Date()
      );

      const { id, title, description, userId, dueDate } = newNote;

      const savedNote = { id, title, description, userId, dueDate };

      this.notes.push(savedNote);
      this.saveNotesToFile();
      return savedNote;
    } catch (error: any) {
      console.log(error.message);
    }
  }

  async readNotes(userId: number): Promise<Note[]> {
    return this.notes.filter((note) => note.userId === userId);
  }

  @logDecorator()
  async updateNote(
    noteId: number,
    updatedNote: interNotes
  ): Promise<string | undefined> {
    try {
      const index = this.notes.findIndex((note) => note.id === noteId);
      if (index !== -1) {
        const { title, description, userId } = updatedNote;

        if (title) this.notes[index].title = title;
        if (description) this.notes[index].description = description;
        if (userId) this.notes[index].userId = userId;
        this.saveNotesToFile();

        return `Note con ID ${noteId} actualizada.`;
      } else {
        throw new Error(`No se encontró ninguna nota con el ID ${noteId}`);
      }
    } catch (error: any) {
      console.log(error.message);
    }
  }

  @logDecorator()
  async deleteNote(noteId: number): Promise<string | undefined> {
    try {
      const index = this.notes.findIndex((note) => note.id === noteId);
      if (index !== -1) {
        this.notes.splice(index, 1);
        this.saveNotesToFile();
        return `Note con ID ${noteId} eliminado.`;
      } else {
        throw new Error(`No se encontró ninguna nota con el ID ${noteId}`);
      }
    } catch (error: any) {
      console.log(error.message);
    }
  }
}
