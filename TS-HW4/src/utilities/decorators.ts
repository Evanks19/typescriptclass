import { LogApp } from './../services/logServices';

import 'reflect-metadata';
const requiredMetadataKey = Symbol('required');

export function required(
  target: Object,
  propertyKey: string | symbol,
  parameterIndex: number
) {
  let existingRequiredParameters: number[] =
    Reflect.getOwnMetadata(requiredMetadataKey, target, propertyKey) || [];
  existingRequiredParameters.push(parameterIndex);
  Reflect.defineMetadata(
    requiredMetadataKey,
    existingRequiredParameters,
    target,
    propertyKey
  );
}

export function validate(
  target: any,
  propertyName: string,
  descriptor: TypedPropertyDescriptor<any>
) {
  let method = descriptor.value as Function;
  descriptor.value = function () {
    let requiredParameters: number[] = Reflect.getOwnMetadata(
      requiredMetadataKey,
      target,
      propertyName
    );
    if (requiredParameters) {
      for (let parameterIndex of requiredParameters) {
        if (
          parameterIndex >= arguments.length ||
          arguments[parameterIndex] === undefined
        ) {
          throw new Error('Missing required argument.');
        }
      }
    }

    return method.apply(this, arguments);
  };
}

export function logDecorator() {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value;

    descriptor.value = async function (...args: any[]) {
      try {
        const logApp = new LogApp();

        let method;
        const service: string = propertyKey.includes('User') ? 'User' : 'Notes';

        const id = args[0];

        const result = await originalMethod.apply(this, args);

        if (result) {
          if (propertyKey === 'createUser' || propertyKey === 'createNote') {
            method = 'POST';
          } else if (
            propertyKey === 'updateUser' ||
            propertyKey === 'updateNote'
          ) {
            method = 'UPDATE';
          } else {
            method = 'DELETE';
          }

          await logApp.createLog(id, method, service);
          return result;
        }

        // return await originalMethod.apply(this, args);
      } catch (error: any) {
        console.log(error.message);
      }
    };
  };
}

export function noteValidator(target: any, propertyKey: string) {
  let value = target[propertyKey];

  const getter = function () {
    return value;
  };

  const setter = function (newValue: any) {
    if (newValue.length < 3) {
      throw new Error('Title must have at least 3 characters');
    }
    value = newValue;
  };

  Object.defineProperty(target, propertyKey, {
    get: getter,
    set: setter,
    enumerable: true,
    configurable: true,
  });
}

export function emailValidator(target: any, propertyKey: any) {
  let value: any = target[propertyKey];

  const getter = (): void => {
    return value;
  };

  const setter = (newValue: string): void => {
    const validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
    if (!validEmail.test(newValue)) {
      throw new Error('Not valid email');
    }
    value = newValue;
  };

  Object.defineProperty(target, propertyKey, {
    get: getter, // aquí definimos el getter
    set: setter, // aquí definimos el setter
    enumerable: true, // la propiedad se puede iterar
    configurable: true, // l
  });
}
