import { emailValidator } from "../utilities/decorators";

export class User {
  id: number;

  name: string;

  lastname: string;

  @emailValidator
  email: string;

  

  constructor(id: number, name: string, lastname: string, email: string) {
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.email = email;
  }
}


