import { noteValidator } from "../utilities/decorators";

export class Note {
  id: number;

  @noteValidator
  title: string;

  description: string;

  userId: number;

  dueDate: Date;

  constructor(
    id: number,
    title: string,
    description: string,
    userId: number,
    dueDate: Date
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.userId = userId;
    this.dueDate = dueDate;
  }
}
