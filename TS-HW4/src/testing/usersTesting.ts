import { UserApp } from '../services/userServices';
import { User } from '../models/user';

async function testUserAp(method: string) {

  interface UpdateUser {
    name?: string;

    lastname?: string;

    email?: string;
  }

  const userApp = new UserApp();

  switch (method) {
    case 'add':
      // const newUser: User = new User(1, 'kevin', 'gonzalez', 'user@mail.com');

      const newUser = await userApp.createUser(
        3,
        'evanks',
        'gaitan',
        'evanks@mail.com'
      );
      if (newUser) console.log('newUser :>> ', newUser);
      break;

    case 'list':
      const allUsers = await userApp.readUsers();
      console.log('allUsers :>> ', allUsers);
      break;

    case 'update':
     

      const userIdToUpdate = 2;
      const updatedNote: UpdateUser = {
        name: 'kenley',
        lastname: 'evanks',
      };
      const updatedUser = await userApp.updateUser(userIdToUpdate, updatedNote);
      if (updatedUser) console.log(updatedUser);
      break;

    case 'delete':
      const deletedUser = await userApp.deleteUser(2);
      if (deletedUser) console.log(deletedUser);
      break;
  }
}

testUserAp('add');
