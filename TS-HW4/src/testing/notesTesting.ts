import { NotesApp } from '../services/notesServices';
import { Note } from '../models/notes';

async function testNotesApp(method: string) {
  interface UpdatedNotes {
    title?: string;

    description?: string;

    userId?: number;

    dueDate?: Date;
  }

  const notesApp = new NotesApp();

  switch (method) {
    case 'add':
      //  Crear una nueva nota
      // const newNote: Note = new Note(
      // 1,
      // "Tí",
      // "Descripción de la nota",
      // 1,
      // new Date()
      // );
      // await notesApp.createNote(newNote);

      const newNote = await notesApp.createNote(
        5,
        'tercera nota de evanks',
        'prueba 5',
        3
      );
      if (newNote) console.log('newNote :>> ', newNote);
      break;

    case 'list':
      const userId = 3;
      const userNotes = await notesApp.readNotes(userId);
      console.log(`Notas del usuario ${userId}:`, userNotes);
      break;

    case 'update':
      const noteToUpdate = 1;

      const newNoteUpdate: UpdatedNotes = {
        title: 'new title',
      };

      const updateNote = await notesApp.updateNote(noteToUpdate, newNoteUpdate);
      if (updateNote) console.log(updateNote);
      break;

    case 'delete':
      const noteIdToDelete = 1;
      const deletedNote = await notesApp.deleteNote(noteIdToDelete);
      if (deletedNote) console.log(deletedNote);

      break;
  }
}

testNotesApp('list');
