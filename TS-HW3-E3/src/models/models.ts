export namespace Models {
  export class Item<T> {
    constructor(public name: string, public price: T) {}
  }
}
