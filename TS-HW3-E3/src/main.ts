// / <reference path="./utilities/utilities.ts" />
// / <reference path="./models/models.ts" />
import { Utilities } from "./utilities/utilities";
import { Models } from "./models/models";

const clampedValue = Utilities.clamp(199, 10,30 );
console.log("Clamped value:", clampedValue);

const shuffledArray = Utilities.shuffle([1, 2, 3, 4, 5]);
console.log("Shuffled array:", shuffledArray);

const items = [
  new Models.Item<number>("Producto 1", 10),
  new Models.Item<number>("Producto 2", 20),
  new Models.Item<number>("Producto 3", 30),
];
console.log("Items:", items);

const numbers = [1, 2, 3, 4, 5];
const averageValue = Utilities.average(numbers);
console.log("Average value:", averageValue);
