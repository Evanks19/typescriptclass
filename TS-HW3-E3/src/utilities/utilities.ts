export namespace Utilities {
  export function shuffle<T>(array: T[]): T[] {
    return array.sort(() => Math.random() - 0.5);
  }

  export function clamp<T extends number>(val: T, min: T, max: T): T {
    return Math.max(min, Math.min(max, val)) as T;
  }

  export function average<T extends number>(array: T[]): number {
    const total = array.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
      0
    );
    return total / array.length;
  }
}
