import { LogApp } from '../services/logServices';

export function logDecorator() {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value;

    descriptor.value = async function (...args: any[]) {
      try {
        const logApp = new LogApp();

        let method;
        let id;
        let service;

        const result = await originalMethod.apply(this, args);
        if (result) {
          if (propertyKey === 'createEmployee') {
            method = 'POST';
            id = args[0].id;
            service = args[1];
          } else if (propertyKey === 'updateEmployee') {
            method = 'UPDATE';
            id = args[0];
            service = args[2];
          } else {
            method = 'DELETE';
            id = args[0];
            service = args[1];
          }

          await logApp.createLog(id, method, service);
          return result;
        }
      } catch (error: any) {
        console.log(error.message);
      }
    };
  };
}

export function validateLegth() {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
      try {
        const name =
          propertyKey === 'createEmployee' ? args[0]['name'] : args[1]['name'];

        if (name.length <= 3) {
          throw new Error('Name must have at least 3 characters');
        } else {
          return originalMethod.apply(this, args);
        }
      } catch (error: any) {
        console.log(error.message);
      }

     
    };
  };
}
