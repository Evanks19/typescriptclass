import { employee } from '../models/employee';
import { manager } from '../models/manager';

export type EmployeeWithSalary = employee & {salary:number};

export type EmployeeIntert = employee & { internshipDuration:number}

export type EmployeBase = EmployeeWithSalary | EmployeeIntert 


export type jsonType<T> = T extends manager ? 'manager':'intern' 
