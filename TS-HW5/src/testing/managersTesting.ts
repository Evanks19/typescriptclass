import { manager } from '../models/manager';
import { EmployeesApp } from '../services/employeeService';

async function testManagerApp(method: string) {
  const employeeManagersApp = new EmployeesApp<manager>('manager');

  switch (method) {
    case 'add':
      const newManager = await employeeManagersApp.createEmployee(
        {
          id: 2,
          name: 'joshue',
          age: 20,
          role: 'seller ',
          department: 'ventas',
          salary: 700,
        },
        'manager'
      );

      if (newManager) console.log(newManager);

      break;

    case 'list':
      const allManagers = await employeeManagersApp.readEmployees();
      if (allManagers) console.log(allManagers);
      break;

    case 'update':
      const updatedManagers = await employeeManagersApp.updateEmployee(
        2,
        {
          id: 2,
          name: 'joshua',
          age: 22,
          role: 'seller ',
          department: 'ventas',
          salary: 750,
        },
        'manager'
      );

      if (updatedManagers) console.log(updatedManagers);

      break;

    case 'delete':
      const deleteManager = await employeeManagersApp.deleteEmployee(
        2,
        'manager'
      );
      if (deleteManager) console.log(deleteManager);
      break;
  }
}

testManagerApp('add');
