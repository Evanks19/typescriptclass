import { intern } from '../models/intern';
import { EmployeesApp } from '../services/employeeService';

async function testInternApp(method: string) {
  const employeeInternsApp = new EmployeesApp<intern>('intern');

  switch (method) {
    case 'add':
      const newIntern = await employeeInternsApp.createEmployee(
        {
          id: 2,
          name: 'kevin',
          age: 26,
          role: 'pasante',
          internshipDuration: 4,
        },
        'intern'
      );

      if (newIntern) console.log(newIntern);
      break;

    case 'list':
      const allInterns = await employeeInternsApp.readEmployees();
      if (allInterns) console.log(allInterns);
      break;

    case 'update':
      const updatedIntern = await employeeInternsApp.updateEmployee(
        2,
        {
          id: 2,
          name: 'kenley',
          age: 30,
          role: 'pasante',
          internshipDuration: 2,
        },
        'intern'
      );
      if (updatedIntern) console.log(updatedIntern);

      break;

    case 'delete':
      const deletedIntern = await employeeInternsApp.deleteEmployee(
        2,
        'intern'
      );

      if (deletedIntern) console.log(deletedIntern);
      break;
  }
}

testInternApp('add');
