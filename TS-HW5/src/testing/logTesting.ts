import { LogApp } from '../services/logServices';

async function testApp(method: string) {
  interface UpdatedNotes {
    title?: string;

    description?: string;

    userId?: number;

    dueDate?: Date;
  }

  const logApp = new LogApp();

  switch (method) {
    case 'list':
      const logs = await logApp.readLogs();
      console.log(logs);
      break;

    case 'delete':
      await logApp.deleteLogs();
      console.log([]);
      break;
  }
}

testApp('list');
