import { EmployeeWithSalary } from "../types/employeeTypes";
import { Rols } from "../types/roles";

export class manager implements EmployeeWithSalary{
    id:number;
    name: string;
    age: number;
    role: Rols;
    salary: number;
    department: string;


    constructor(id:number,name:string, age:number, role:Rols,salary:number, department:string) {
        this.id=id;
        this.name = name;
        this.age = age;
        this.role = role;
        this.salary = salary;
        this.department = department;

    }
}