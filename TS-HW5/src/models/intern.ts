import { EmployeeIntert } from '../types/employeeTypes';
import { Rols } from '../types/roles';

export class intern implements EmployeeIntert {
  id: number;
  name: string;
  age: number;
  role: Rols;
  internshipDuration: number;

  constructor(
    id: number,
    name: string,
    age: number,
    role: Rols,
    internshipDuration: number
  ) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.role = role;
    this.internshipDuration = internshipDuration;
  }
}
