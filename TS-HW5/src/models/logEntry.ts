export class LogEntry {
  action: string;
  date: Date;

  constructor(action: string,date:Date) {
    this.action = action;
    this.date = date
  }
}
