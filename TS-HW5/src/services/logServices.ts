import { LogEntry } from '../models/logEntry';
import fs from 'fs';
import path from 'path';

const logsFilePath = path.resolve(__dirname, '../../data/logs.json');

export class LogApp {
  private logs: LogEntry[] = [];

  constructor() {
    this.loadLogsFromFile();
  }

  private loadLogsFromFile() {
    try {
      const data = fs.readFileSync(logsFilePath, 'utf-8');
      this.logs = JSON.parse(data);
    } catch (error) {
      this.logs = [];
    }
  }

  private saveLogsToFile() {
    fs.writeFileSync(logsFilePath, JSON.stringify(this.logs, null, 2));
  }

  async createLog(id: number, method: string, service: string): Promise<void> {
    const message = `${service}: ${id} was ${method}${
      method === 'POST' ? 'ED' : 'D'
    }`;

    const newLog: LogEntry = new LogEntry(message, new Date());
    this.logs.push(newLog);
    this.saveLogsToFile();
  }

  async readLogs(): Promise<LogEntry[] | [] | null> {
    return this.logs;
  }

  async deleteLogs(): Promise<void> {
    this.logs = [];
    this.saveLogsToFile();
  }
}
