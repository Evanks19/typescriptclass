import fs from 'fs';
import path from 'path';
import { manager } from '../models/manager';
import { intern } from '../models/intern';
import { jsonType } from '../types/employeeTypes';
import { logDecorator, validateLegth } from '../utilities/decorators';

export class EmployeesApp<T extends manager | intern> {
  private employees: T[] = [];

  constructor(file: jsonType<T>) {
    this.loadNotesFromFile(file);
  }

  private loadNotesFromFile(file: jsonType<T>) {
    try {
      const employeeFilePath = path.resolve(
        __dirname,
        `../../data/${file}s.json`
      );
      const data = fs.readFileSync(employeeFilePath, 'utf-8');
      this.employees = JSON.parse(data);
    } catch (error) {
      this.employees = [];
    }
  }

  private saveNotesToFile(file: jsonType<T>) {
    const employeeFilePath = path.resolve(__dirname, `../../data/${file}s.json`);
    fs.writeFileSync(employeeFilePath, JSON.stringify(this.employees, null, 2));
  }

  async readEmployees(): Promise<T[]> {
    return this.employees;
  }

  @validateLegth()
  @logDecorator()
  async createEmployee(
    newEmployee: T,
    key: jsonType<T>
  ): Promise<T | undefined> {
    try {
      this.employees.push(newEmployee);
      this.saveNotesToFile(key);
      return newEmployee;
    } catch (error: any) {
      console.log(error.message);
    }
  }

  async updateEmployee(
    employeeId: number,
    updatedEmployee: T,
    key: jsonType<T>
  ): Promise<string | undefined> {
    try {
      const index = this.employees.findIndex(
        (updateEmploye) => updateEmploye.id === employeeId
      );
      if (index !== -1) {
        this.employees[index] = { ...updatedEmployee };

        this.saveNotesToFile(key);

        return `Note con ID ${employeeId} actualizada.`;
      } else {
        throw new Error(`No se encontró ninguna nota con el ID ${employeeId}`);
      }
    } catch (error: any) {
      console.log(error.message);
    }
  }

  async deleteEmployee(
    employeeId: number,
    key: jsonType<T>
  ): Promise<string | undefined> {
    const index = this.employees.findIndex(
      (employeeDeleted) => employeeDeleted.id === employeeId
    );

    try {
      if (index !== -1) {
        this.employees.splice(index, 1);
        this.saveNotesToFile(key);
        return `employee con ID ${employeeId} eliminado.`;
      } else {
        throw new Error(
          `No se encontró ningun employee con el ID ${employeeId}`
        );
      }
    } catch (error: any) {
      console.log(error.message);
    }
  }
}
