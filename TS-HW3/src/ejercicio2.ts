interface Order {
  id: number;
  product: string;
  quantity: number;
  price: number;
}

class OrderManager<T extends Order> {
  private orders: T[];

  constructor() {
    this.orders = [];
  }

  addOrder(order: T): void {
    this.orders.push(order);
  }

  getOrderDetails(orderId: number): T | undefined {
    return this.orders.find((order) => order.id === orderId);
  }

  calculateTotalSales(): number {
    return this.orders.reduce(
      (total, order) => total + order.quantity * order.price,
      0
    );
  }

  applyDiscount(discount: number): void {
    this.orders.forEach((order) => {
      order.price -= order.price * discount;
    });
  }
}

const physicalOrderManager = new OrderManager<Order>();

physicalOrderManager.addOrder({
  id: 1,
  product: "Camisa",
  quantity: 2,
  price: 25,
});
physicalOrderManager.addOrder({
  id: 2,
  product: "Pantalón",
  quantity: 1,
  price: 40,
});

console.log(
  "Detalles de la orden física:",
  physicalOrderManager.getOrderDetails(1)
);
console.log(
  "Total de ventas físicas:",
  physicalOrderManager.calculateTotalSales()
);

physicalOrderManager.applyDiscount(0.1);

console.log(
  "Total de ventas físicas con descuento:",
  physicalOrderManager.calculateTotalSales()
);

interface DigitalOrder extends Order {
  downloadLink: string;
}

const digitalOrderManager = new OrderManager<DigitalOrder>();

digitalOrderManager.addOrder({
  id: 1,
  product: "Ebook",
  quantity: 1,
  price: 10,
  downloadLink: "http://amazon.com",
});

console.log(
  "Detalles de la orden digital:",
  digitalOrderManager.getOrderDetails(1)
);
console.log(
  "Total de ventas digitales:",
  digitalOrderManager.calculateTotalSales()
);
