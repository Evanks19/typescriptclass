namespace ejercicio1 {
  class DataStore<T extends number | string | object> {
    private items: T[];

    constructor() {
      this.items = [];
    }

    // Método para agregar elementos de tipo T
    addItem(item: T): void {
      this.items.push(item);
    }

    // Método para obtener un elemento dado su índice
    getItem(index: number): T | undefined {
      return this.items[index];
    }

    // Método para eliminar un elemento del almacén
    removeItem(index: number): void {
      this.items.splice(index, 1);
    }
  }

  // Ejemplo de uso
  const dataStore = new DataStore();

  //   restriccion
  dataStore.addItem(24);
  dataStore.addItem(19);
  dataStore.addItem('hola');
  dataStore.addItem({ value: 'mundo' });

  const item2 = dataStore.getItem(0);
  if (item2 !== undefined) {
    console.log(item2);
  }

  console.log(dataStore);

  dataStore.removeItem(0);

  console.log(dataStore);
}
