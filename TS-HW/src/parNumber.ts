const handleIsPar = (num:number):boolean => num % 2 === 0

const numExample = 5

const isPar:boolean = handleIsPar(numExample)
console.log(`el numero ${numExample} es ${isPar ? 'par' : 'impar'}`)